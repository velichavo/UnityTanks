using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor.EditorTools;
using UnityEditor;
using UnityEngine.EventSystems;

[EditorTool("Map Editor", typeof(LevelEditor))]
public class CustomMapEditor : EditorTool
{
    [SerializeField] private Texture2D ToolIcon;
    public override GUIContent toolbarIcon
    {
        get
        {
            return new GUIContent
            {
                image = ToolIcon,
                text = "Custom Map Editor Tool",
                tooltip = "Custom Map Editor Tool"
            };
        }
    }

    public override void OnToolGUI(EditorWindow window)
    {
        //EditorGUI.BeginChangeCheck();

        Event currentEvent = Event.current;

        /*if (currentEvent.button == 0 && currentEvent.isMouse)
        {
           Debug.Log(currentEvent.button);
        }*/

        Debug.Log(currentEvent.button);

        if (EditorGUI.EndChangeCheck())
        {

        }
    }
}
