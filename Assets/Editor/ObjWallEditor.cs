using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.EventSystems;

[CustomEditor(typeof(LevelEditor))]
public class ObjWallEditor : Editor
{
    LevelEditor wall;

    public void Awake()
    {
        wall = (LevelEditor)target;
    }
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        if (GUILayout.Button("LoadBrushes"))
        {
            wall.LoadBrush();
        }
    }
}
