using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Tank", menuName = "CustomObj/Tank")]
public class Tank : ScriptableObject
{
    public float Speed, MaxSpeed, Acceleration;
    public Sprite Sprite;
    public Animation Animation;
}
