using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BlankWall", menuName = "CustomObj/BlankWall")]
public class BlankWall : ScriptableObject
{
    public bool isTrigger = false;
    public int WallType;
    public int Depth;
    public float GridWall = 0.25f;
    public float Strength;
    public string WallDescription;
    public Sprite[] WallSprite = new Sprite[1];
    public RuntimeAnimatorController animator;
}
