using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BrushLoader", menuName = "CustomObj/BrushLoader")]
public class BrushLoader : ScriptableObject
{
	public BlankWall[] Wall;
}
