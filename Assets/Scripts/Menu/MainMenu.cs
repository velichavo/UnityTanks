﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine.UI;
using UnityEngine;

public class MainMenu : BaseMenu
{
    
    public void StateSetActive(LevelController controller)
    {
        Time.timeScale = 0;
        Init(levelController);
    }

    protected void Start()
    {
        buttons = GetComponentsInChildren<Button>();
        foreach (var button in buttons)
        {
            BaseButtonMenu baseButtonMenu = button.GetComponent<BaseButtonMenu>();
            if (baseButtonMenu)
            {
                button.onClick.AddListener(baseButtonMenu.Click);
                baseButtonMenu.Init(button, this);
            }
            else
                button.interactable = false;
        }
    }
}

