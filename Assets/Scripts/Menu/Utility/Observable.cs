﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Menu.Utility
{
    public class Observable : IObserver
    {
        private List<IObservable> observables;
        public Observable()
        {
            observables = new List<IObservable>();
        }
        public Observable(IObservable observable)
        {
            observables = new List<IObservable> { observable };
            observable.OnChanged += OnChanged;
        }

        private void OnChanged(object o)
        {

        }

        public void AddObservable(IObservable observable)
        {
            if (!observables.Contains(observable))
            {
                observables.Add(observable);
                observable.OnChanged += OnChanged;
            }
        }

        public void Dispose()
        {
            foreach (var observable in observables)
            {
                observable.OnChanged -= OnChanged;
            }
        }

    }
}
