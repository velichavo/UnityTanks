﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Menu.Utility
{
    public class ObservableVariable<T> : IObservable
    {
        public event Action<object> OnChanged;

        private T item;
        public T Item 
        { 
            get => item;
            set
            {
                item = value;
                OnChanged?.Invoke(value);
            }
        }

        public ObservableVariable()
        {
            Item = default;
        }

        public ObservableVariable(T defaultItem)
        {
            Item = defaultItem;
        }
    }
}
