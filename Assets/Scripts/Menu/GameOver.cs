using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class GameOver : BaseMenu
{
    [SerializeField] private Text gameOver;
    public void SetStage()
    {
        gameOver.text = Language.TextScenarioGameOver;
    }
    public override void Init(LevelController controller)
    {
        SetStage();
        base.Init(controller);
    }
}
