﻿using System;
using UnityEngine;
using UnityEngine.UI;


public abstract class BaseButtonMenu : MonoBehaviour
{
    public enum SceneIndex {MainMenu, LevelGame, LevelEditor };
    protected Button button;
    protected MainMenu menu;
    public abstract void Click();

    public void Init(Button button, MainMenu menu)
    {
        this.button = button;
        this.menu = menu;
    }

    public void OnDestroy()
    {
        button.onClick.RemoveListener(Click);
    }
}
