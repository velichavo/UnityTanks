using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartStage : BaseMenu
{
    [SerializeField] private Text stageStart;
    public void SetStage()
    {
        stageStart.text = ($"{Language.TextScenarioStartGame} {(1 + LevelController.IndexLoadableLevel)}");
    }
    public override void Init(LevelController controller)
    {
        SetStage();
        base.Init(controller);
    }
}
