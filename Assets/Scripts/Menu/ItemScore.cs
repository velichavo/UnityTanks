using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class ItemScore : MonoBehaviour
{
    private Text textScore;
    private int count, counter, reward;
    private float timer;
    private bool end;

    [SerializeField] private Image imageScore;
    [SerializeField] private float timerScoreItem;
    [SerializeField] private string text;

    public event UnityAction<ItemScore> EndCount;
    public void Init(Sprite image, int reward, int count)
    {
        textScore = GetComponentInChildren<Text>();
        textScore.text = text;
        imageScore.sprite = image;
        this.count = count;
        this.reward = reward;
        counter = 0;
        end = false;
    }
    private void Update()
    {
        if (end) return;

        if (counter <= count)
        {
            if (timer < 0)
            {
                textScore.text = $"{reward * counter}  {text}  {counter}";
                timer = timerScoreItem;
                counter++;
            }
            else timer -= Time.deltaTime;
        }
        else
        {
            end = true;
            EndCount?.Invoke(this);
        }

    }
}
