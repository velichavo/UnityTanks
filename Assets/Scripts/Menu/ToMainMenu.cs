using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ToMainMenu : BaseMenu
{
    public override void Init(LevelController controller)
    {
        base.Init(controller);
        TransitionMainMenu();
        //EndLevelState();
    }
    public static void TransitionMainMenu()
    {
        Time.timeScale = 1;
        LevelController.MenuStatus = 0;
        LevelController.IndexLoadableLevel = 0;
        SceneManager.LoadScene((int)BaseButtonMenu.SceneIndex.MainMenu);
    }
}
