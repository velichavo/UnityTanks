using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scenario : MonoBehaviour
{
    private int currentScenario;
    private LevelController levelController;

    [SerializeField] private BaseMenu[] scenarioItems;

    public void StartScenario(LevelController levelController)
    {
        this.levelController = levelController;
        currentScenario = 0;
        scenarioItems[currentScenario].Init(levelController);
        foreach (var item in scenarioItems)
        {
            item.OnChanged += ChangeScenario;
        }
    }

    private void ChangeScenario()
    {
        currentScenario++;
        if (currentScenario < scenarioItems.Length)
        {
            scenarioItems[currentScenario].Init(levelController);
        }
        else
        {
            foreach (var item in scenarioItems)
            {
                item.OnChanged -= ChangeScenario;
            }
            levelController.State.Item = BaseMenu.LevelState.None;
        }
    }
}
