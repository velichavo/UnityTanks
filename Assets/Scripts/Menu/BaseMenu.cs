using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

public abstract class BaseMenu : MonoBehaviour
{
    public event UnityAction OnChanged;

    protected float timer;
    protected Button[] buttons;
    protected int currentMenu;
    protected LevelController levelController;
    [SerializeField] protected float levelTransitionTime;
    [SerializeField] protected bool waitForPlayerAction;
    public LevelController LevelController { get => levelController; }

    public enum LevelState : int { None, GameOver, NextStage, PauseMenu, NewGame }
    private void Update()
    {
        if (waitForPlayerAction) return;
        if (timer > 0)
            timer -= Time.deltaTime;
        else
        {
            EndLevelState();
        }
    }
    public virtual void Init(LevelController controller)
    {
        timer = levelTransitionTime;
        gameObject.SetActive(true);
        levelController = controller;
    }
    public void EscapeMenu()
    {
        Time.timeScale = 1;
        EndLevelState();
    }
    protected void EndLevelState()
    {
        OnChanged?.Invoke();
        gameObject.SetActive(false);
    }
}
