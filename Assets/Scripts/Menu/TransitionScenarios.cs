using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts.Menu.Utility;


public class TransitionScenarios : MonoBehaviour
{
    [SerializeField] private MainMenu mainMenu;
    [SerializeField] private Scenario nextStage;
    [SerializeField] private Scenario gameOver;
    [SerializeField] private Scenario newGame;
    [SerializeField] private LevelController levelController;

    private void Start() 
    {
        if (levelController)
        {
            levelController.State.OnChanged += OnChanged;
        }
        else Debug.LogError("�� �������� LevelController");
    }
    private void OnChanged(object o)
    {
        BaseMenu.LevelState lc = (BaseMenu.LevelState)o;
        switch (lc)
        {
            case BaseMenu.LevelState.None:
                mainMenu.EscapeMenu();
                break;
            case BaseMenu.LevelState.GameOver:
                gameOver.StartScenario(levelController);
                break;
            case BaseMenu.LevelState.NextStage:
                nextStage.StartScenario(levelController);
                break;
            case BaseMenu.LevelState.PauseMenu:
                mainMenu.StateSetActive(levelController);
                break;
            case BaseMenu.LevelState.NewGame:
                newGame.StartScenario(levelController);
                break;

        }
    }
    public void OnDestroy()
    {
        levelController.State.OnChanged -= OnChanged;
    }
}
