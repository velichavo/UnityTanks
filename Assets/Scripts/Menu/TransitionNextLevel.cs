using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransitionNextLevel : BaseMenu
{
    public override void Init(LevelController levelController)
    {
        base.Init(levelController);
        levelController.StartLevel();
    }
}
