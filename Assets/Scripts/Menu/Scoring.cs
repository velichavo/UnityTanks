using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

public class Scoring : BaseMenu
{
    private int index, countKilledEnemies;
    private IEnumerator wait;
    private float height;
    private float indentTopY = 170, indentBottomY = 500;
    private float indentItemScore;
    private bool isNextItemScore;
    private List<ItemScore> itemScores;

    [SerializeField] private float delayScore;
    [SerializeField] private ItemScore prefabItemScore;

    protected void Start()
    {
        wait = Wait();
    }
    public override void Init(LevelController controller)
    {
        base.Init(controller);
        timer = delayScore;
        index = 0;
        isNextItemScore = false;
        TransitionScenarios ts = GetComponentInParent<TransitionScenarios>();
        RectTransform rt = ts.GetComponent<RectTransform>();
        height = rt.rect.height;
        itemScores = new List<ItemScore>();
    }

    private void Update()
    {
        if(index < levelController.Enemies.Count)
            if (timer < 0)
            {
                countKilledEnemies = levelController.IdKilledEnemies.FindAll(FindIndexEnemie).Count;
                ItemScore itemScore = Instantiate(prefabItemScore, transform);
                itemScore.Init(levelController.Enemies[index].TankEnemieUI, levelController.Enemies[index].Reward, countKilledEnemies);
                itemScore.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, - indentTopY - index * (indentBottomY - indentTopY - LevelController.Enemies.Count * itemScore.GetComponent<RectTransform>().rect.height) /
                    (levelController.Enemies.Count <= 1 ? 1 : levelController.Enemies.Count - 1));
                itemScore.EndCount += NextItemScore;
                itemScores.Add(itemScore);
                timer = delayScore;
                isNextItemScore = true;
            }
            else 
                if(!isNextItemScore) timer -= Time.deltaTime;


    }
    private void NextItemScore(ItemScore itemScore)
    {
        index++;
        isNextItemScore = false;
        itemScore.EndCount -= NextItemScore;
        if (index == levelController.Enemies.Count)
            if (!waitForPlayerAction) StartCoroutine(wait);

    }
    private bool FindIndexEnemie(int index)
    {
        return this.index == index;
    }

    private IEnumerator Wait()
    {
        yield return new WaitForSeconds(levelTransitionTime);
        foreach (var itemScore in itemScores)
        {
            Destroy(itemScore.gameObject);
        }
        EndLevelState();
    }
}
