using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonContinue : BaseButtonMenu
{
    public override void Click()
    {
        if (SceneManager.GetActiveScene().buildIndex == (int)SceneIndex.LevelGame)
        {
            menu.LevelController.State.Item = BaseMenu.LevelState.None;
        }
    }
}
