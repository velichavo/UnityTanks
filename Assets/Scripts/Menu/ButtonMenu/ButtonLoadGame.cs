using UnityEngine;

public class ButtonLoadGame : BaseButtonMenu
{
    public override void Click()
    {
        Debug.Log("LoadGame");
        button.interactable = false;
    }
}
