﻿using UnityEngine.SceneManagement;

public class ButtonEditor : BaseButtonMenu
{
    public override void Click( )
    {
        LevelController.MenuStatus = 3;
        SceneManager.LoadScene((int)SceneIndex.LevelEditor);
    }
}

