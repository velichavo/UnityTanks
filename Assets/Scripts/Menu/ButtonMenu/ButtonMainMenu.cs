using UnityEngine.SceneManagement;
using UnityEngine;

public class ButtonMainMenu : BaseButtonMenu
{
    public override void Click()
    {
        ToMainMenu.TransitionMainMenu();
    }
}
