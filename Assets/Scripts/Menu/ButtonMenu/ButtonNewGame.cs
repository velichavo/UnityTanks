﻿using UnityEngine.SceneManagement;

public class ButtonNewGame : BaseButtonMenu
{
    public override void Click()
    {
        LevelController.MenuStatus = 1;
        LevelController.IndexLoadableLevel = 0;
        SceneManager.LoadScene((int)SceneIndex.LevelGame);
    }
}

