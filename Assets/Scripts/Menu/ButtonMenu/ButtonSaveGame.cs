using UnityEngine;

public class ButtonSaveGame : BaseButtonMenu
{
    public override void Click()
    {
        Debug.Log("SaveGame");
        button.interactable = false;
    }
}
