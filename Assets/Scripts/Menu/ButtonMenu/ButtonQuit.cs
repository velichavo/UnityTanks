﻿using UnityEngine;

public class ButtonQuit : BaseButtonMenu
{
    public override void Click()
    {
        Application.Quit();
    }
}

