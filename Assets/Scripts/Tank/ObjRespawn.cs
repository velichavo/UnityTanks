using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjRespawn : MonoBehaviour
{
    public Animator animator;
    private float timer;

    public delegate void OnSpawnEnemie();
    public event OnSpawnEnemie IsSpawnEnemie;

    public void RespawnActive(float timer)
    {
        animator.SetBool("Play", true);
        this.timer = timer;
    }

    private void Update()
    {
        if (timer < 0)
        {
            IsSpawnEnemie?.Invoke();
            animator.SetBool("Play", false);
            gameObject.SetActive(false);
        }
        else
            timer -= Time.deltaTime;
    }
}
