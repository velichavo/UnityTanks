using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Enemie : objTank
{
    float timer;
    protected int idPrefab;
    public event UnityAction<Enemie> IsTakeDamegeEnemie;

    public int IdPrefab { get => idPrefab; }
    public void Init(int idPrefab, ObjRespawn respawnPoint)
    {
        this.idPrefab = idPrefab;
        if (respawnPoint)
            this.respawnPoint = respawnPoint;

    }
    public void SpawnTank(float activeTime)
    {
        respawnPoint.gameObject.SetActive(true);
        respawnPoint.RespawnActive(activeTime);
    }
    protected override void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    protected override void Update()
    {
        Move(direction);
        base.Update();
        if (Input.GetKeyDown(KeyCode.R))
            TakeDamege(1);
    }

    private void FixedUpdate()
    {
        if (timer < 0)
        {
            float rnd = Mathf.Floor(UnityEngine.Random.Range(0f, 3.9f));
            switch (rnd)
            {
                case 0:
                    direction = new Vector2(1, 0);
                    break;
                case 1:
                    direction = new Vector2(-1, 0);
                    break;
                case 2:
                    direction = new Vector2(0, 1);
                    break;
                case 3:
                    direction = new Vector2(0, -1);
                    break;
            }
            timer = UnityEngine.Random.Range(0f, 4f);
        }
        else timer -= Time.deltaTime;
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        timer = 0;
    }

    public override void TakeDamege(float damage)
    {
        base.TakeDamege(damage);
        if (currentWeapon > 0) return;
        hp--;
        if (hp <= 0)
        {
            if (explosion != null)
                Instantiate<Explosion>(explosion, transform.position, transform.rotation);
            IsTakeDamegeEnemie?.Invoke(this);
            Destroy(gameObject);
        }
    }
    public override void NextWeapon()
    {
        if (currentWeapon < objWeapons.Count - 1)
        {
            base.NextWeapon();
            return;
        }
        hp++;
    }    
}
