using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDamaged 
{
    public TWall WallType { get; }

    public enum TWall { None = 0, Brick = 1, Betton = 2, Water = 4, Ice = 5, Wood = 6, Tank = 7 };
}
