using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Player : objTank
{
    bool moveHorizontal, moveVertical;

    public event UnityAction<float> AddLife;
    public event UnityAction<float> RemoveLive;
    
    protected override void Start()
    {
        base.Start();
    }
    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
        if (Input.GetMouseButton(0))
        {
            Shot();
        }
        if (Input.GetKeyDown(KeyCode.T))
            TakeDamege(1);
    }
    private void FixedUpdate()
    {
        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");

        moveHorizontal = true;
        moveVertical = true;

        if (h != 0 && moveVertical)
        {
            moveVertical = false;
            v = 0;
        }
        if (v != 0 & moveHorizontal)
        {
            moveHorizontal = false;
            h = 0;
        }

        direction = new Vector2(h, v);

        Move(direction);
    }
    public override void TakeDamege(float damage)
    {
        if (currentWeapon > 0)
            base.TakeDamege(damage);
        else
        { 
            hp--;
            RemoveLive?.Invoke(hp);
            if (explosion)
                Instantiate<Explosion>(explosion, transform.position, transform.rotation);
            gameObject.SetActive(false);
        }
    }
    public override void NextWeapon()
    {
        if (currentWeapon < objWeapons.Count - 1)
        {
            base.NextWeapon();
            return;
        }
        hp++;
        AddLife?.Invoke(hp);
    }
}
