using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallBrush : MonoBehaviour
{
    private Sprite wallSprite;
    private int id;
    private SpriteRenderer spriteRenderer;

    public Sprite WallSprite { get => wallSprite; set => wallSprite = value; }
    public SpriteRenderer SpriteRenderer { get => spriteRenderer; set => spriteRenderer = value; }
    public int Id { get => id; set => id = value; }

    private void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }
}
