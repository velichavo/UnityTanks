using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEditor;
using UnityEngine.UI;
using System;

public class LevelEditor : MonoBehaviour, IPointerMoveHandler
{
    [SerializeField] private BrushLoader brushLoader;
    [SerializeField] private int currentBrush;
    [SerializeField] private BlankWall[] blankWalls;
    [SerializeField] private WallButton buttonWall;
    [SerializeField] private Image buttonPanel;
    [SerializeField] private Text textPosition;
    [SerializeField] private LoadLevel loadLevel;
    [SerializeField] private LevelButton levelButton;
    [SerializeField] private RectTransform transformNameButton;
    [SerializeField] private Text inputFieldPath;
    [SerializeField] private Transform pixel;

    private WallBrush wallBrush;
    private int WallType;
    private string WallDescription;
    private Sprite WallSprite;
    private WallButton currentWallButton;
    private Storage storage;

    private List<WallButton> listWallButton;
    private GameData[] gameDatas;
    private List<ObjWall> objWalls;

    private List<LevelButton> levelsButtons;

    public void LoadBrush()
    {
        blankWalls = new BlankWall[brushLoader.Wall.Length];
        for (int i = 0; i < brushLoader.Wall.Length; i++)
        {
            blankWalls[i] = brushLoader.Wall[i];
        }
    }

    private void CreateButton()
    {
        listWallButton = new List<WallButton>();
        for (int i = 0; i < brushLoader.Wall.Length; i++)
        {
            if (buttonWall)
            {
                WallButton button = Instantiate<WallButton>(buttonWall, buttonPanel.transform);
                button.PositionButton = new Vector2(i % 2 * 53 + 33, i / 2 * -53 - 105);
                button.DescriptionWall = brushLoader.Wall[i].WallDescription;
                button.SpriteWall = brushLoader.Wall[i].WallSprite[0];
                button.Id = i;
                button.OnClick += ButtonSelected;
            }
            else Debug.Log("�� ������ ������ ������");
        }
    }
    private void CreateLevelsButton()
    {
        if (listWallButton != null)
            while (listWallButton.Count > 0)
                RemoveLevelButton(listWallButton[0]);

        listWallButton = new List<WallButton>();
        loadLevel.GetDyrectory();
        for (int i = 0; i < loadLevel.PathLevels.Length; i++)
        {
            if (levelButton)
            {
                LevelButton button = Instantiate<LevelButton>(levelButton, transformNameButton.transform);
                button.Init(i, loadLevel.PathLevels[i]);
                button.OnClick += ButtonLevelSelected;
            }
            else Debug.Log("�� ������ ������ ������");
        }
    }
    private void RemoveLevelButton(WallButton wallButton)
    {
        listWallButton.Remove(wallButton);
        Destroy(wallButton.gameObject);
    }
    private void ButtonLevelSelected(LevelButton levelButton)
    {
        inputFieldPath.text = loadLevel.PathLevels[levelButton.Index];
        loadLevel.SetLevel(levelButton.TextLevelName);
    }
    private void ButtonSelected(WallButton wallButton)
    {
        if (currentWallButton)
            currentWallButton.ButtonComponent.interactable = true;
        currentWallButton = wallButton;
    }
    private int GetIndexSprite(Vector3 position)
    {
        float i = brushLoader.Wall[currentWallButton.Id].GridWall;
        Vector3 gridPosition = GetGridPosition(position);
        Vector3 remainder = new Vector3(Mathf.Abs(gridPosition.x % 1 / i), Mathf.Abs(gridPosition.y % 1 / i));
        return (int)(remainder.x + remainder.y * 1 / i);
    }
    private Vector3 GetGridPosition(Vector3 position)
    {
        float i = brushLoader.Wall[currentWallButton.Id].GridWall;
        return new Vector3(Mathf.Floor(position.x / i) * i, Mathf.Floor(position.y / i) * i, 0);
    }
    private void Start()
    {
        CreateButton();
        CreateLevelsButton();
        loadLevel.LoadButton += CreateLevelsButton;
        objWalls = new List<ObjWall>();
        wallBrush = GetComponentInChildren<WallBrush>();
        if (!wallBrush) Debug.LogError("����������� BrushWall");
    }
    private void FixedUpdate()
    {
        if (!loadLevel.ObjWall)
        {
            Debug.LogError("ObjWall �� �����");
            return;
        }

        if (Input.GetMouseButton(0) && currentWallButton)
        {
            bool hit = false;
            bool hitWall = false;
            float grid = brushLoader.Wall[currentWallButton.Id].GridWall;
            Vector2 position = GetGridPosition(Camera.main.ScreenToWorldPoint(Input.mousePosition));
            string s = "";

            for (int i = 0; i < (LevelController.GridSize * grid); i++)
            {
                RaycastHit2D[] r = Physics2D.RaycastAll(
                    new Vector2(position.x + i / LevelController.GridSize + 0.5f / LevelController.GridSize, 
                        position.y + 0.5f / LevelController.GridSize),
                    Vector2.up, 
                    grid  - 1 / LevelController.GridSize);
                s += (position.x + i / LevelController.GridSize / 2 + 0.5f / LevelController.GridSize) + ", " + (position.y + 0.5f / LevelController.GridSize) + "\n";
                foreach (var item in r)
                {
                    if (item.transform == transform) hit = true;
                    if (item.transform != transform) hitWall = true;
                }
            }

            textPosition.text = position.x + ", " + position.y + "\n" + s;

            if (hit && !hitWall)
            {
                loadLevel.AddWall(position, GetIndexSprite(Camera.main.ScreenToWorldPoint(Input.mousePosition) - new Vector3(-6.5f, -6.5f)), brushLoader.Wall[currentWallButton.Id].WallType);
            }
        }

        if (Input.GetMouseButton(1))
        {
            RaycastHit2D r = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.up, 0);
            ObjWall wall = r.transform.GetComponent<ObjWall>();
            if (wall)
            {
                loadLevel.RemoveWall(wall);
            }
            
        }
    }
    public void OnPointerMove(PointerEventData eventData)
    {
        if (!currentWallButton || !wallBrush) return;

        wallBrush.transform.position = GetGridPosition(Camera.main.ScreenToWorldPoint(Input.mousePosition));

        wallBrush.SpriteRenderer.sprite = brushLoader.Wall[currentWallButton.Id].WallSprite[GetIndexSprite(Camera.main.ScreenToWorldPoint(Input.mousePosition) - new Vector3(-6.5f, -6.5f))];
    }


}
