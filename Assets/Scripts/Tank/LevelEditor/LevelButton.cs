using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class LevelButton : MonoBehaviour
{
    public event UnityAction<LevelButton> OnClick;
    private string textLevelName;
    private int index;
    private Button buttonComponent;

    public int Index { get => index; }
    public string TextLevelName { get => textLevelName; }

    void Start()
    {
        GetComponentInChildren<Text>().text = textLevelName;
        buttonComponent = GetComponent<Button>();
        buttonComponent.onClick.AddListener(OnClickButon);
        float height = buttonComponent.image.rectTransform.rect.height;
        buttonComponent.image.rectTransform.anchoredPosition = new Vector2(0, height * (-index - 1));
    }
    public void Init(int index, string name)
    {
        this.index = index;
        textLevelName = name;
    }

    public void OnClickButon()
    {
        OnClick?.Invoke(this);
    }
    private void OnDestroy()
    {
        buttonComponent.onClick.RemoveListener(OnClickButon);
    }
}
