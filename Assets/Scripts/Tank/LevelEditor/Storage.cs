using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class Storage 
{
    private string filePath;
    private BinaryFormatter formatter;

    public Storage(string fileName)
    {
        //filePath = Application.persistentDataPath + "/save/" + fileName;
        filePath = fileName;
        InitBinaryFormater();
    }

    private void InitBinaryFormater()
    {
        formatter = new BinaryFormatter();
        SurrogateSelector selector = new SurrogateSelector();
        Vector3Surrogate vector3Surrogate = new Vector3Surrogate();
        QuaternionSurrogate quaternionSurrogate = new QuaternionSurrogate();

        selector.AddSurrogate(typeof(Vector3), new StreamingContext(StreamingContextStates.All), vector3Surrogate);
        selector.AddSurrogate(typeof(Quaternion), new StreamingContext(StreamingContextStates.All), quaternionSurrogate);

        formatter.SurrogateSelector = selector;
    }

    public object[] Load(object[] saveDataByDefault)
    {
        if (!File.Exists(filePath))
            if (saveDataByDefault != null)
            {
                Save(saveDataByDefault);
                return saveDataByDefault;
            }
        FileStream file = File.Open(filePath, FileMode.Open);
        object[] savedData = (object[])formatter.Deserialize(file);
        file.Close();

        return savedData;
    }

    public void Save(object[] saveData)
    {
        Debug.Log(filePath);
        FileStream file = File.Create(filePath);
        formatter.Serialize(file, (object)saveData);
        file.Close();
    }
}
