using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WallButton : MonoBehaviour
{
    public delegate void OnAction(WallButton wallButton);
    public event OnAction OnClick;

    private Sprite spriteWall;
    private string descriptionWall;
    private int typeWall;
    private int id;
    private Image imageButton;
    private Text textWall;
    private Vector2 positionButton;
    private Button buttonComponent;

    public int TypeWall
    {
        get => typeWall;
        set => typeWall = value;
    }
    public string DescriptionWall { get => descriptionWall; set => descriptionWall = value; }
    public Sprite SpriteWall { get => spriteWall; set => spriteWall = value; }
    public Vector2 PositionButton { get => positionButton; set => positionButton = value; }
    public Button ButtonComponent { get => buttonComponent; set => buttonComponent = value; }
    public int Id { get => id; set => id = value; }
    private void Start()
    {
        imageButton = GetComponent<Image>();
        imageButton.sprite = spriteWall;
        imageButton.rectTransform.anchoredPosition = positionButton;
        textWall = GetComponentInChildren<Text>();
        textWall.text = descriptionWall;
        buttonComponent = GetComponent<Button>();

    }
    public void OnClickButon()
    {
        buttonComponent.interactable = false;
        OnClick?.Invoke(this);
    }
    private void OnDestroy()
    {
        buttonComponent.onClick.RemoveListener(OnClickButon);
    }
}