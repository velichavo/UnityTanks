using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

[Serializable]
public class GameData
{
    public int Type, IndexSprite;
    public Vector3 position;
    public Quaternion rotation;
}

