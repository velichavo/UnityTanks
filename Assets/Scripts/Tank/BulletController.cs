using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BulletController : MonoBehaviour
{
    [SerializeField] private float damageMultiplier;
    [SerializeField] private GameObject explosion;
    private float speed;
    private float damage;
    private objTank tankTag;
    private Rigidbody2D rb2d;
    private void Update()
    {
        RaycastHit2D[] raycastHit2D = Physics2D.RaycastAll(transform.position, transform.right, speed * Time.deltaTime);
        transform.Translate(Vector3.right * speed * Time.deltaTime);
        if(raycastHit2D.Length > 0)
            Hit(raycastHit2D);
    }
    private void Hit(RaycastHit2D[] collisions)
    {
        if (collisions.Length > 1) Debug.Log("�������� ������ 2�");
        foreach (var collision in collisions)
        {
            IDamaged damaged = collision.transform.GetComponent<IDamaged>();
            if (damaged != null)
            {
                switch (damaged.WallType)
                {
                    case IDamaged.TWall.Water:
                        break;
                    case IDamaged.TWall.Ice:
                        break;
                    case IDamaged.TWall.Wood:
                        break;
                    case IDamaged.TWall.Tank:
                        TakeDamageTank(collision);
                        break;
                    case IDamaged.TWall.None:
                        Kill();
                        break;
                    default:
                        TakeDamageWall();
                        break;
                }
            }
            
        }
    }
    private void TakeDamageWall()
    {
        ObjWall wall;
        Vector3 v2 = transform.position - transform.up * 0.375f;
        RaycastHit2D[] hit = Physics2D.RaycastAll(new Vector2(v2.x, v2.y), transform.up, 0.75f);
        for (int i = 0; i < hit.Length; i++)
        {
            wall = hit[i].transform.GetComponent<ObjWall>();
            if (wall)
            {
                switch (wall.WallType)
                {
                    case IDamaged.TWall.Brick:
                        wall.TakeDamage(damage);
                        Kill();
                        break;
                    case IDamaged.TWall.Betton:
                        wall.TakeDamage(damage);
                        Kill();
                        break;
                }
            }
        }
    }
    private void TakeDamageTank(RaycastHit2D collision)
    {
        objTank tank = collision.transform.GetComponent<objTank>();
        if (tank)
        {
            if (tank != tankTag)
            {
                tank.TakeDamege(damage * damageMultiplier);
                Kill();
            }
        }
    }
    private void Kill()
    {
        if (explosion != null)
            Instantiate(explosion, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }

    public void Init(objTank tank, float speed, float damage)
    {
        tankTag = tank;
        this.speed = speed;
        this.damage = damage;
    }
}
