using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class objTank : MonoBehaviour, IDamaged
{
    [SerializeField] protected float maxSpeed = 1;
    [SerializeField] protected float deltaSpeed = 0.3f;
    [SerializeField] protected float hp = 1;
    [SerializeField] private int reward;
    [SerializeField] protected Explosion explosion;
    [HideInInspector] public Transform Weapon;
    [HideInInspector] public ObjWeapon sWeapon;
    protected Vector2 direction;
    protected Quaternion quaternionDirection = new Quaternion(0, 0, 0, 1);

    protected BoxCollider2D collider;
    protected Rigidbody2D rigidbody;
    protected List<ObjWeapon> objWeapons;

    protected ObjRespawn respawnPoint;

    public event UnityAction IsShooting;

    [SerializeField] protected int currentWeapon;
    [SerializeField] protected List<ObjWeapon> PrefabWeapons;
    [SerializeField] private Sprite tankEnemieUI;
    public float MaxSpeed 
    {
        get => maxSpeed;
        set => maxSpeed = value;
    }

    public IDamaged.TWall WallType => IDamaged.TWall.Tank;

    public Sprite TankEnemieUI { get => tankEnemieUI; }
    public int Reward { get => reward;}

    // Start is called before the first frame update
    protected virtual void Start()
    {
        collider = GetComponent<BoxCollider2D>();
        rigidbody = GetComponent<Rigidbody2D>();
        CreateWeapon();
        objWeapons[currentWeapon].gameObject.SetActive(true);
    }

    // Update is called once per frame
    protected virtual void Update()
    {   
    }
    protected void CreateWeapon()
    {
        objWeapons = new List<ObjWeapon>();
        foreach (var weapon in PrefabWeapons)
        {
            ObjWeapon obj = Instantiate(weapon, transform);
            obj.Init(this);
            objWeapons.Add(obj);
            obj.gameObject.SetActive(false);
        }
    }

    protected void Shot()
    {
        IsShooting?.Invoke();
    }

    public virtual void NextWeapon()
    {
        objWeapons[currentWeapon].gameObject.SetActive(false);
        currentWeapon++;
        objWeapons[currentWeapon].gameObject.SetActive(true);
        maxSpeed += deltaSpeed;
    }

    protected void Move(Vector2 direction)
    {

        if (direction.x != 0)
        {
            quaternionDirection = direction.x > 0 ? new Quaternion(0, 0, 0, 1) : new Quaternion(0, 0, 1, 0);
            transform.position = new Vector3(transform.position.x, Mathf.Round(transform.position.y * LevelController.GridSize) / LevelController.GridSize, 0);

        }
        if (direction.y != 0)
        {
            quaternionDirection = direction.y > 0 ? new Quaternion(0, 0, 0.7f, 0.7f) : new Quaternion(0, 0, -0.7f, 0.7f);
            transform.position = new Vector3(Mathf.Round(transform.position.x * LevelController.GridSize) / LevelController.GridSize, transform.position.y, 0);

        }

        transform.rotation = quaternionDirection;
        rigidbody.velocity = direction * MaxSpeed;
    }

    public virtual void TakeDamege(float damage)
    {
        if (currentWeapon > 0)
        {
            objWeapons[currentWeapon].gameObject.SetActive(false);
            currentWeapon--;
            objWeapons[currentWeapon].gameObject.SetActive(true);
            maxSpeed -= deltaSpeed;
            return;
        }
    }
}
