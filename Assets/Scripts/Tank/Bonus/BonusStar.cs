using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusStar : ObjBonus
{
    protected override void Start()
    {
        base.Start();
        Id = Bonus.Star;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        objTank tank = collision.gameObject.GetComponent<objTank>();
        if (tank)
        {
            tank.NextWeapon();
            Destroy(gameObject);
        }
        
    }
}
