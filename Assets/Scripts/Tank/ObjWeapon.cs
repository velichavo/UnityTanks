using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class ObjWeapon : MonoBehaviour
{
    [SerializeField] float shootingTime;
    [SerializeField] float damage;
    [SerializeField] ObjBullet bullet;
    [SerializeField] float bulletSpeed;
    FireBarrel fireBarrel;
    private objTank tankTag;
    ShootPoint shootPoint;
    float timer = 0;
    bool shoot;

    public objTank TankTag { get => tankTag; }

    public void Init(objTank tank)
    {
        tankTag = tank;
        tankTag.IsShooting += Shoot;
    }
    void Start()
    {
        shootPoint = GetComponentInChildren<ShootPoint>();
        fireBarrel = GetComponentInChildren<FireBarrel>();
        fireBarrel.gameObject.SetActive(false);
    }

    public void Shoot()
    {
        if (timer > 0 || !gameObject.activeSelf) return;

        if (fireBarrel)
            fireBarrel.transform.gameObject.SetActive(true);
        if (bullet)
        {
            ObjBullet sblt = Instantiate<ObjBullet>(bullet, shootPoint.transform.position, shootPoint.transform.rotation);
            sblt.Init(TankTag, bulletSpeed, damage);
            timer = shootingTime;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (timer > 0)
            timer -= Time.deltaTime;

    }

    private void OnDestroy()
    {
        TankTag.IsShooting -= Shoot;
    }
}
