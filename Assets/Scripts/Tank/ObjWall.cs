using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjWall : MonoBehaviour, IDamaged
{
    [SerializeField] private Sprite wallSprite;
    private bool isTrigger;
    private int spriteIndex;
    private IDamaged.TWall wallType;
    private float grid = 0.25f;
    private float strength;
    private SpriteRenderer spriteRenderer;
    private BoxCollider2D collider2D;

    [SerializeField] private BrushLoader brushLoader;

    public IDamaged.TWall WallType { get => wallType; }
    public Sprite WallSprite { get => WallSprite; }
    public float Grid { get => grid; }
    public int SpriteIndex { get => spriteIndex; }
    public float Strength { get => strength; }

    private void Start()
    {
        if (wallType == IDamaged.TWall.None) return;
        spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRenderer.sprite = wallSprite;
        collider2D = GetComponent<BoxCollider2D>();
        collider2D.offset = new Vector2(grid / 2, grid / 2);
        collider2D.size = new Vector2(grid, grid);
        collider2D.isTrigger = isTrigger;
    }

    public void Init(Vector2 position, int indexSprite, int type)
    {
        wallType = (IDamaged.TWall)type;
        spriteIndex = indexSprite;

        for (int i = 0; i < brushLoader.Wall.Length; i++)
        {
            if((int)wallType == brushLoader.Wall[i].WallType)
            {
                wallSprite = brushLoader.Wall[i].WallSprite[indexSprite];
                grid = brushLoader.Wall[i].GridWall;
                transform.position = new Vector3(position.x, position.y, brushLoader.Wall[i].Depth);
                isTrigger = brushLoader.Wall[i].isTrigger;
                strength = brushLoader.Wall[i].Strength;
                if (brushLoader.Wall[i].animator != null)
                {
                    Animator animator = gameObject.AddComponent<Animator>();
                    animator.runtimeAnimatorController = brushLoader.Wall[i].animator;
                }
            }
        }
    }

    public void TakeDamage(float damage)
    {
        if (damage >= Strength)
            Destroy(gameObject);
    }
}
