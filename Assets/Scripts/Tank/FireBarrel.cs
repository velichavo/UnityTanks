using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class FireBarrel : MonoBehaviour
{
    [SerializeField] float burningTime = 0.2f;
    float timer = 0.2f;
    void Update()
    {
        if (timer > 0)
            timer -= Time.deltaTime;
        else
            gameObject.SetActive(false);
    }

    private void OnEnable()
    {
        timer = burningTime;
    }
}
