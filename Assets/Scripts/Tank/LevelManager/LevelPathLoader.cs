﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Tank
{
     public class LevelPathLoader
    {
        private string pathLevels;
        private string[] Levels;
        public LevelPathLoader(string pathLevels)
        {
            this.pathLevels = pathLevels;
        }
        public string[] LoadLevel()
        {
            if (!File.Exists(pathLevels))
            {
                return null;
            }
            return File.ReadAllLines(pathLevels);
        }
    }
}
