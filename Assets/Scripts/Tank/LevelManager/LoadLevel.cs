using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using Assets.Scripts.Tank;
using UnityEngine.Events;

public class LoadLevel : MonoBehaviour
{
    public event UnityAction LoadButton;

    private Storage storage;
    private string[] pathLevels;

    private LevelPathLoader LevelPathLoader;

    private GameData[] gameDatas;
    private List<ObjWall> objWalls;

    [SerializeField] ObjWall objWall;
    [SerializeField] private string currentLevel;
    [SerializeField] private string pathFileLevels;
    [SerializeField] private string levelDyrectory;

    public ObjWall ObjWall { get => objWall; }
    public string[] PathLevels { get => pathLevels; }

    public void LoadData()
    {
        if (currentLevel.Length == 0)
        {
            Debug.Log("��� ������");
            return;
        }
        storage = new Storage(levelDyrectory + currentLevel);
        gameDatas = new GameData[objWalls.Count];
        gameDatas = (GameData[])storage.Load(gameDatas);
        if (objWalls != null)
            ClearLevel();
        objWalls = new List<ObjWall>();
            for (int i = 0; i < gameDatas.Length; i++)
        {
            AddWall(gameDatas[i].position, gameDatas[i].IndexSprite, gameDatas[i].Type);
        }
    }

    public void SaveData(Text fileName)
    {
        string fn = fileName.text;
        if (fn.Length == 0)
            fn = currentLevel;

        storage = new Storage(levelDyrectory + fn);
        gameDatas = new GameData[objWalls.Count];
        for (int i = 0; i < objWalls.Count; i++)
        {
            gameDatas[i] = new GameData();
            gameDatas[i].position = objWalls[i].transform.position;
            gameDatas[i].rotation = objWalls[i].transform.rotation;
            gameDatas[i].Type = (int)objWalls[i].WallType;
            gameDatas[i].IndexSprite = objWalls[i].SpriteIndex;
        }

        storage.Save(gameDatas);
        LoadButton?.Invoke();
    }
    public void SetLevel(string level)
    {
        currentLevel = level;
    }
    public ObjWall AddWall(Vector2 position, int indexSprite, int type)
    {
        ObjWall wall = Instantiate<ObjWall>(ObjWall, transform);
        wall.Init(position, indexSprite, type);
        objWalls.Add(wall);
        return wall;
    }
    public void RemoveWall(ObjWall wall)
    {
        objWalls.Remove(wall);
        if(wall != null)
            Destroy(wall.gameObject);
    }
    private void ClearLevel()
    {
        while (objWalls.Count > 0)
            RemoveWall(objWalls[0]);
    }
    public void GetDyrectory()
    {
        pathLevels = Directory.GetFiles(levelDyrectory);
        for (int i = 0; i < pathLevels.Length; i++)
        {
            pathLevels[i] = pathLevels[i].Split('/')[1];
        }
    }
    public void LoadLevelStart()
    {
        if (LevelController.MenuStatus == 1)
        {
            LevelPathLoader = new LevelPathLoader(pathFileLevels);
            pathLevels = LevelPathLoader.LoadLevel();
            currentLevel = pathLevels[LevelController.IndexLoadableLevel];
            LoadData();
        }
    }
    private void Start()
    {
        if (objWalls == null)
            objWalls = new List<ObjWall>();
    }

}
