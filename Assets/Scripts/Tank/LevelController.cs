using Assets.Scripts.Menu.Utility;
using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Unity.Mathematics;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class LevelController : MonoBehaviour
{
    #region static
    public static int CellSize = 48;
    //������ ������
    public static float GridSize = 4;
    // 0 - none
    // 1 - New Game
    // 2 - Load Game
    // 3 - Level Editor
    public static int MenuStatus;
    public static int IndexLoadableLevel;

    #endregion
    private float timer;
    private bool whileSpawn, isPaused;
    private List<ObjRespawn> respawnPoint;
    private Queue<Enemie> enemiesInstance;
    private List<Enemie> enemiesSpawned;
    private Queue<Image> uiEnemies;
    private List<Player> players;
    private List<int> idKilledEnemies;//id ����� ������ ������, ����� ��� �������� �����
    private int idPrefab;
    private IEnumerator endlevel;
    private ObservableVariable<BaseMenu.LevelState> state = new ObservableVariable<BaseMenu.LevelState>(BaseMenu.LevelState.None);
    [SerializeField] private Player playerPrefab;
    [SerializeField] private List<Enemie> enemies;
    [SerializeField] private int enemiesCountMax;
    [SerializeField] private int enemiesSpawnedCountMax;
    [SerializeField] private float spawnTime;
    [SerializeField] private float activeTime;
    [SerializeField] private Text lifeAmount;
    [SerializeField] private Image uiPanel;
    [SerializeField] private Image uiEnemiePrefab;
    [SerializeField] private Transform playerSpawner;
    [SerializeField] private LoadLevel loadLevel;

    public ObservableVariable<BaseMenu.LevelState> State { get => state; set => state = value; }
    public List<int> IdKilledEnemies { get => idKilledEnemies;}
    public List<Enemie> Enemies { get => enemies; }

    private void Start()
    {
        MenuStatus = 1;
        state.Item = BaseMenu.LevelState.NewGame;
        IndexLoadableLevel = 0;
        respawnPoint = GetComponentsInChildren<ObjRespawn>().ToList();
        SetEnemieSpawners(respawnPoint);
        endlevel = EndLevel();
        StartLevel();
    }
    public void StartLevel()
    {
        if(enemiesInstance != null )
            foreach (var enemie in enemiesInstance) Destroy(enemie);
        enemiesInstance = new Queue<Enemie>();
        if (enemiesSpawned != null )
            foreach (var enemie in enemiesSpawned) Destroy(enemie);
        enemiesSpawned = new List<Enemie>();
        if (uiEnemies != null )
            foreach (var uiEnemie in uiEnemies) Destroy(uiEnemie);
        uiEnemies = new Queue<Image>();
        idKilledEnemies = new List<int>();
        PlayerCreate();
        CreateStackEnemies();
        loadLevel.LoadLevelStart();
    }
    private void PlayerCreate()
    {
        if (players == null)
        {
            players = new List<Player>();
            Player player = Instantiate<Player>(playerPrefab,
                new Vector3(playerSpawner.transform.position.x, playerSpawner.transform.position.y, playerPrefab.transform.position.z), playerSpawner.rotation);
            players.Add(player);
            player.AddLife += AddLive;
            player.RemoveLive += RemoveLive;
        }
        else
            foreach (var player in players)
            {
                player.gameObject.SetActive(true);
                player.transform.position = new Vector3(playerSpawner.transform.position.x, playerSpawner.transform.position.y, playerPrefab.transform.position.z);
                player.transform.rotation = playerSpawner.rotation;
            }
    }
    private void SetEnemieSpawners(List<ObjRespawn> respawn)
    {
        foreach (var point in respawn)
        {
            point.IsSpawnEnemie += OnActiveEnemie;
            point.gameObject.SetActive(false);
        }
    }
    private void CreateStackEnemies()
    {
        for (int i = 0; i < enemiesCountMax; i++)
        {
            int point = Mathf.FloorToInt(UnityEngine.Random.Range(0, respawnPoint.Count - 0.1f));
            Enemie enemie = Instantiate<Enemie>(enemies[idPrefab], respawnPoint[point].transform.position, respawnPoint[point].transform.rotation);
            enemie.Init(idPrefab, respawnPoint[point].GetComponent<ObjRespawn>());
            enemie.IsTakeDamegeEnemie += TakeDamageEnemie;
            enemie.gameObject.SetActive(false);
            enemiesInstance.Enqueue(enemie);

            if (uiEnemiePrefab)
            {
                Image image = Instantiate<Image>(uiEnemiePrefab, uiPanel.transform);
                image.rectTransform.anchoredPosition = new Vector2(i % 2 * 32 + 18, i / 2 * -32 - 60);
                image.sprite = enemie.TankEnemieUI;
                uiEnemies.Enqueue(image);
            }
            else print("UI Enemie Prefab �� �����");
        }
    }
    private void RemoveLive(float hp)
    {
        if (hp > 0)
            StartCoroutine(SpawnPlayer());
        else
            state.Item = BaseMenu.LevelState.GameOver;
    }
    private void AddLive(float hp) => lifeAmount.text = hp.ToString();

    private IEnumerator SpawnPlayer()
    {
        yield return new WaitForSeconds(2f);
        PlayerCreate();
    }
    private void TakeDamageEnemie(Enemie item)
    {
        idKilledEnemies.Add(item.IdPrefab);         // ��������� ����
        item.IsTakeDamegeEnemie -= TakeDamageEnemie;
        enemiesSpawned.Remove(item);
        if (enemiesSpawned.Count == 0 && enemiesInstance.Count == 0)
            StartCoroutine(endlevel);
    }
    private IEnumerator EndLevel()
    {
        yield return new WaitForSeconds(2f);
        idKilledEnemies.Sort();
        foreach (var player in players)
            player.gameObject.SetActive(false);
        if (IndexLoadableLevel < loadLevel.PathLevels.Length)
        {
            IndexLoadableLevel++; //��������� �������
            state.Item = BaseMenu.LevelState.NextStage;
        }
        else
        {
            //congratulations
        }

    }
    private void Update()
    {
        if (state.Item != BaseMenu.LevelState.None) return;

        if (enemiesSpawned.Count < enemiesSpawnedCountMax && enemiesInstance.Count > 0)
            if (timer < 0)
            {
                Enemie enemie = enemiesInstance.Peek();
                enemie.SpawnTank(activeTime);
                whileSpawn = true;
                timer = spawnTime;
            }
            else
                if (!whileSpawn) timer -= Time.deltaTime;

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if(state.Item == BaseMenu.LevelState.None)
            {
                state.Item = BaseMenu.LevelState.PauseMenu;
            }
            else
            {
                state.Item = BaseMenu.LevelState.None;
            }
        }
    }
    public void OnActiveEnemie()
    {
        if (enemies[0])
        {
            Image image = uiEnemies.Dequeue();
            Destroy(image.gameObject);
            Enemie enemie = enemiesInstance.Dequeue();
            enemie.gameObject.SetActive(true);
            enemiesSpawned.Add(enemie);
            whileSpawn = false;
        }
        else
            print("������ ������ ����");
    }
    private void OnDestroy()
    {
        foreach (var player in players)
        {
            player.AddLife -= AddLive;
            player.RemoveLive -= RemoveLive;
        }
        foreach (var point in respawnPoint)
            point.IsSpawnEnemie -= OnActiveEnemie;
    }

    /*private void OnDrawGizmos()
    {
        Gizmos.color = Color.gray;

        for (int i = 0; i < collider2D.points.Length - 1; i++)
        {
            Gizmos.DrawLine(new Vector3(collider2D.points[i].x + transform.position.x, collider2D.points[i].y + transform.position.y, 0), new Vector3(collider2D.points[i + 1].x + transform.position.x, collider2D.points[i + 1].y + transform.position.y, 0));
        }
    }*/
}